# README #
Proyecto Prueba 

Este documento es la guia de instalacion y configuracion para el proyecto prueba.A continuacion encontrara una rapida descripcion de los componentes utilizados asi como de la forma como deben ser instalados para su uso.

Componentes e instalacion

Sistema operativo

Esta montado en sistemas operativos unix; probado en ubuntu server y redhat. Con conocimiento en sistemas operativos windows su instalacion y configuracion no debera tener ningun problema.

Ambiente Virtual 

El proyecto debe residir dentro de un ambiente virtual especificamente para su uso. El ambiente virtual debe estar montado bajo:

 - Python 3.6+

Base de datos

Respecto a la base de datos se ha seleccionado el motor de base de datos relacional postgresql:

 - Postgresql 9.6+

Framewor de desarrollo 

Respecto al framework de desarrollo se cuenta con django.

 - Django 2.1
 
Versionado 

Para el tema de versionado se utilizaran dos ramas -master -desarrollo

La rama master tendra el codigo de produccion y nunca se debera hacer un commit a esta en una maquina de desarrollo.
Todos los commits de desarrollo se realizaran a la rama desarrollo.

Realice el procedimiento normal para poner los archivos en stagging git add .

 -Realice el procedimiento normal para poner los archivos en stagging git add .
 -Antes de hacer commit haga un git status y verifique lo que le muestra ahi fue lo que usted hizo.
 -Una vez verificado eso, haga un commit y luego un push a la rama desarrollo.

$ git commit -m"nombre del desarrollo"
$ git push origin desarrollo

Librerias

Para conocer las librerias que se hacen necesarias para el proyecto se procedera a revisar el archivo requirements.txt en el que se encuentan especificadas
